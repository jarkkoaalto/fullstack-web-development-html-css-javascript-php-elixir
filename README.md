# FULLSTACK-Web-Development-HTML-CSS-JavaScript-php

This course is about FULL-STACK web development where we would cover 3 fundamental FRONT-END languages HTML5, CSS3, and JavaScript required for Client-side Static web development, and learn BACK-END languages PHP7 and PERL, with SQL with MySQL. We would cover all the languages and test them by writing files, and sample projects like LOGIN Page, Extracting Information from Database, HTML FORM, etc.

You will learn ELIXIR programming along with PHP where you would learn a wide range of utilities, and a powerful programming language for server and desktop implementation.

Couse content:

###### Section 01: HTML
###### Section 02: CSS (Cascading Style Sheet)
###### Section 03: JavaScript
###### Section 04: PHP (Hypertext Preprocessor)
###### Section 05: SQL (Structured Query Language)
###### Section 06: Welcome to Elixir
###### Section 07: Basic Syntax , Variables and Operators
###### Section 08: Decision Making Statement
###### Section 09: String
###### Section 10: List and Tuples
###### Section 11: Maps
###### Section 12: Modules and Functions
###### Section 13: Recursion and Loops
###### Section 14: Additional Topics