a = false
unless a === false do
    IO.puts"condition is not satisfied unless statement" 
else
    IO.puts"condition is  satisfied unless statement" 
end
IO.puts"Outside the unless statement"

b = false
if b === false do
    IO.puts"Condition is not satisfied if statement"
else 
    IO.puts"Condition is satisfied if statement"
end
IO.puts"Outside the if statement"