defmodule User do 
    defstruct name: "John", age: 27
end

john = %User{}
# john right now is : %User{age:27, name:"John"}

# To access name and age of John,
IO.puts(john.name)
IO.puts(john.age)