# create new process

send(self(), {:hello, "Hi there"})

receive do
    {:hello, msg} -> IO.puts(msg)
    {:another_case, msg} -> IO.puts("This one won't match!")
end