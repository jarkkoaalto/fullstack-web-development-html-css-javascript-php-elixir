IO.puts(is_list('Hello')) # True use ''-> true if you used "" -> false
IO.puts(length([]))
IO.puts(length([3,4,5]))

# Concatenate list
IO.puts(['1','2','3'] ++ ['4','5','6'])

# First element
l = ['1','2','3']
IO.puts(hd(l))

# first and second element
IO.puts(tl(l))

# List last element
IO.puts(List.last(l))