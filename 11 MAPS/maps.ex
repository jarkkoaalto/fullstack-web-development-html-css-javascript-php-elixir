# Maps are the "go to" data structure in Elixir
# Compalre to the keyword in the list

map=%{:a=>"b",2=>:b}
IO.puts(map[:a])
IO.puts(map[2])

# Adding new value 
new_map=Dict.put_new(map,:new_val,"value")
IO.puts(new_map[:new_val])

%{:a=>a}=%{:a=>1,2=>:b} # a = 1
IO.puts(a)

map_new = %{:a=>1,2=>:b}
IO.puts(map_new.a)