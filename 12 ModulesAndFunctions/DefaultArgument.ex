defmodule Greeter do 
    def hello(name, country \\ "en") do
        phrase(country) <> name
    end

    defp phrase("en"), do: "Hello, "
    defp phrase("es"), do: "Hola, "
end

# ERLANG command promt  arguments
# Greeter.hello("Joe", "en")
# Greeter.hello("Joe")
# Greeter.hello("Joe","es")