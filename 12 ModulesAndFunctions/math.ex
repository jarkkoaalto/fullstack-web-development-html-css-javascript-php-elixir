sum = fn(a,b) -> a+b end
IO.puts(sum.(1,5))

sum = &(&1 + &2)
IO.puts(sum.(2,4))

# Named function
defmodule Math do 
    def sum(a,b) do
        a+b
    end
end
IO.puts(Math.sum(5,6))
