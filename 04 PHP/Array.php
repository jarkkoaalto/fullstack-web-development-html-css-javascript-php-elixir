<!DOCTYPE html>
<html>
    <head>
    <title>PHP Array</title> 
    </head>
    <body>
    
    <?php
    $families = array("Driffin" => array(
        "Peter",
        "Lois",
        "Megan"
        ),
        "Quagmire" => array(
            "Glenn"
        ),
        "Brown" => array(
            "Cleveland",
            "Loretta",
            "Junior"
        )
    );
    
    echo "Is " . $families['Griffin'][2] . " a part of the Griffin falmily? <br>";

    // While loop
    $i = 1;
    while($i <= 5) {
        echo "The number is " . $i . "<br />";
        i++;
    }

    // do - while loop
    $j = 1;
    do {
        $j++;
        echo "The number is " . $j . "<br />";
    }
    while($j <= 5);

    // For loop

    for($u = 1; $u<=5; $u++) {
        echo "The number is " .$u . "<br />";
    }

    // foreach

    $x = array("one","two","three");
    foreach($x as $value) {
        echo $value . "<br />";
    }

    ?>
    
    </body>
</html>