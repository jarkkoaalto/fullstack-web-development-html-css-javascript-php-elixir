<!DOCTYPE html>
<html>
    <head>
    <title>PHP Functions</title> 
    </head>
    <body>
    
    <?php
        // function example
        function writeName(){
            echo "Firstname Lastname";
        }

        echo "My name is "
        writeName();
      
         // another example
         function printName($fname) {
             echo $fname . " Refsnes.<br />";
         }

         echo "My name is ";
         printName("Kai Jim");
         echo "My sister's name is ";
         printName("Hege");
         echo "My brother's name is ";
         printName("Stale");

         // Another example
         function multibleName($fname, $punctuation) {
             echo $fname . " Refsnes " . $punctuation . "<br />";
         }
         
         echo " My name is ";
         multibleName("Kai Jim", ".");
         echo "My sister's name is ";
         multibleName("Hege","!");
         echo "My broter's name is ";
         multibleName("Ståle","?");
         
      ?>
         <!-- Form example -->
      <form action="function.php" method="post">
         <p>Your name: <input type="text" name="name" /></p>
         <p>Your age: <input type="text" name="age" /></p>
         <p><input type="submit" /></p>
      </form>
        Hi <?php echo htmlspecialchars($_POST['name']); ?>
        You are <?php echo (int)$_POST['age']; ?> years old.

    </body>
</html>