# Basics string and print output
a = "Hello World"
IO.puts(a)
IO.puts(String.length(a))

# Convatenate two String
b = "xyz"
c = "uvw #{b}"
IO.puts(c)

v = b<>"-"<>c # xyz-uvw xyz
IO.puts(v)

# String reverse 
IO.puts(String.reverse(v)) # zyx wvu-zyx


if a===b do
        IO.puts"Same"
else
        IO.puts"Not Same"
end

# Match
IO.puts(String.match?("abcd",~r/ab/)) # true
IO.puts(String.match?("abcd",~r/ee/)) # false