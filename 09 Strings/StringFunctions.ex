# String to list
IO.puts(String.graphemes("Hi There")) 

# Duplicate
IO.puts(String.duplicate("A",3))

# Downcase
IO.puts("Hi There lowercase is")
IO.puts(String.downcase("Hi There"))

# replace
IO.puts("replaced abcd string")
IO.puts(String.replace("abcd","a","w"))

IO.puts(Atom.to_string(:hello))

# Upprecase
IO.puts(String.upcase("qwertyuiop"))

# search letter
IO.puts(String.at("qwertyuiop",7))

# slice
IO.puts("slice sting: QWERTYUIOP")
IO.puts(String.slice("QWERTYUIOP",0,5))

IO.puts(String.first("QWERTYUIOP"))
IO.puts(String.last("QWERTYUIOP"))